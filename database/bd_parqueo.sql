-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 20, 2023 at 01:51 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bd_parqueo`
--

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `placa` varchar(11) NOT NULL,
  `ci` varchar(15) NOT NULL,
  `nombres` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `telefono` int(11) NOT NULL,
  `descripcionvehiculo` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`placa`, `ci`, `nombres`, `apellidos`, `telefono`, `descripcionvehiculo`) VALUES
('111-ABC', '10101010', 'jose', 'miranda', 69677638, 'camioneta blanca'),
('1224DDG', '6313213', 'Moises', 'Caicedo', 72895565, 'peta azul'),
('1234ABC', '5663818', 'Paco', 'Lopez', 17171717, 'volkswagen'),
('1452ACV', '22', 'Harum tempora iste v', 'Minus ut possimus i', 16, 'Mollitia necessitati'),
('1872DDG', '50505050', 'Manuel', 'Padilla', 72556432, 'Camioneta azul'),
('1992ELI', '44', 'Eli', 'Lima', 60304099, 'peta'),
('222-ABC', '20202020', 'Isabel', 'Pantoja', 60454032, 'Jeep negro'),
('3333AAA', '43', 'Suscipit autem eveni', 'Aut eum aut quam aut', 63, 'Provident voluptate'),
('3333ABC', '30303030', 'Marco Antonio', 'Etcheverry', 67584123, 'Ferrari rojo'),
('3432VAO', '2314123', 'Andres', 'Iniesta', 1733299, 'Motocicleta roja'),
('4321AFA', '49', 'Est ducimus ration', 'Autem similique culp', 36, 'Quis nulla dolor est'),
('4333BAR', '1132312', 'Leonel', 'Messi', 62738291, 'Camion blanco'),
('4444CAR', '14', 'Carlos', 'Victoria', 21, 'TANQUE'),
('5656GHJ', '5238741', 'Antonio', 'Cruz', 2352311, 'Quadratrac verde'),
('6666AAA', '545454', 'Eliana', 'Loza', 60304099, 'camioneta'),
('7515JME', '345623', 'Adriana', 'Salvatierra', 60456452, 'Hummer amarilla'),
('7687REA', '72313229', 'Sergio', 'Ramos', 72412512, 'Chevrolet negro'),
('7777GAS', '96', 'Israel', 'Marcani', 86, 'peta'),
('A inventore', '5', 'Est tempora beatae a', 'Esse dolore adipisc', 80, 'Ad minima ullam pari'),
('Accusamus n', '26', 'Pariatur Molestiae ', 'Autem et qui eiusmod', 57, 'Sapiente sequi aliqu'),
('Alias qui p', '960', 'Soluta reprehenderit', 'Elit qui exercitati', 73, 'Sunt unde ad ea qui '),
('Corporis de', '42', 'Praesentium voluptat', 'Error non porro volu', 77, 'Optio culpa harum '),
('Dolore cons', '643', 'Dolore incidunt ut ', 'Voluptate mollitia n', 59, 'Ipsa libero omnis d'),
('Eos sint ni', '40', 'Reiciendis nisi adip', 'Rerum id voluptatem', 4, 'Quaerat tenetur inve'),
('Est id eum ', '20', 'Expedita quod beatae', 'Numquam sapiente rat', 3, 'Nemo saepe excepturi'),
('Non nobis f', '635', 'Obcaecati sunt dolor', 'Aut vitae dicta sed ', 29, 'Dicta voluptas quia '),
('Numquam ill', '32', 'Et veniam ipsam vol', 'Aliqua Dolore corru', 34, 'Ipsum in non ducimu'),
('Pariatur No', '100', 'Nostrud dolor suscip', 'Rerum dolor dolorem ', 4, 'In commodi porro off'),
('Tempor in d', '64', 'Est quibusdam esse ', 'Sit sed ad cupidata', 37, 'Architecto nesciunt'),
('Tenetur quo', '25', 'Pariatur Dolores fu', 'Adipisci sapiente ra', 38, 'Atque maiores sit im');

-- --------------------------------------------------------

--
-- Table structure for table `parqueo`
--

CREATE TABLE `parqueo` (
  `idparqueo` int(6) NOT NULL,
  `placa` varchar(11) NOT NULL,
  `fecha` date NOT NULL,
  `horaentrada` time NOT NULL,
  `horasalida` time NOT NULL,
  `estado_parqueo` int(5) NOT NULL,
  `costo_total` double NOT NULL,
  `observaciones` varchar(120) NOT NULL,
  `estado_noti` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `parqueo`
--

INSERT INTO `parqueo` (`idparqueo`, `placa`, `fecha`, `horaentrada`, `horasalida`, `estado_parqueo`, `costo_total`, `observaciones`, `estado_noti`) VALUES
(34, '111-ABC', '2023-08-24', '07:40:00', '19:09:26', 0, 13, 'reserva rapida', 1),
(82, '222-ABC', '2023-09-10', '10:47:30', '07:40:16', 0, 141.48, 'reserva rapida', 0),
(83, 'Tempor in d', '2023-09-10', '11:52:52', '15:00:00', 0, 2, 'Similique ad occaeca', 0),
(84, 'Numquam ill', '2023-09-10', '11:53:09', '20:00:00', 0, 7, 'Dolore ut delectus ', 0),
(85, 'Pariatur No', '2023-09-10', '14:17:01', '15:00:00', 0, 5, 'Et dolorem officia l', 0),
(86, 'Est id eum ', '2023-09-10', '14:17:13', '23:49:00', 0, 7, 'Neque sit praesenti', 0),
(87, 'Alias qui p', '2023-09-10', '15:17:40', '22:00:00', 0, 3, 'In beatae consequatu', 0),
(88, 'Dolore cons', '2023-09-10', '15:18:05', '22:00:00', 0, 5, 'Modi vitae voluptate', 0),
(89, 'A inventore', '2023-09-10', '16:20:36', '21:00:00', 0, 8, 'At id porro tempora', 0),
(90, 'Non nobis f', '2023-09-10', '16:21:23', '23:07:00', 0, 3, 'Est aut nisi sed ips', 0),
(91, 'Accusamus n', '2023-09-10', '17:02:53', '17:19:00', 0, 1, 'Repellendus Veniam', 0),
(92, 'Eos sint ni', '2023-09-10', '17:03:05', '18:05:00', 0, 3, 'Cupiditate autem lab', 0),
(94, '222-ABC', '2023-09-11', '23:43:00', '07:40:16', 0, 141.48, 'reserva rapida', 0),
(95, '111-ABC', '2023-09-12', '00:05:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(96, '222-ABC', '2023-09-12', '00:14:00', '07:40:16', 0, 141.48, 'reserva rapida', 1),
(97, 'Tenetur quo', '2023-09-12', '00:57:23', '07:06:00', 0, 30.72, 'reserva planeada', 0),
(98, 'Corporis de', '2023-09-12', '01:00:27', '02:30:00', 0, 7.46, 'Insitum', 0),
(99, '111-ABC', '2023-09-12', '19:24:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(100, '111-ABC', '2023-09-12', '19:25:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(101, '111-ABC', '2023-09-12', '19:30:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(102, '111-ABC', '2023-09-12', '19:32:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(104, '111-ABC', '2023-09-12', '20:40:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(105, '111-ABC', '2023-09-12', '20:44:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(106, '111-ABC', '2023-09-17', '16:20:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(107, '111-ABC', '2023-09-12', '20:58:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(108, '111-ABC', '2023-09-12', '21:02:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(109, '111-ABC', '2023-09-12', '21:09:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(110, '1234ABC', '2023-09-12', '21:14:32', '21:25:50', 0, 2.2, 'Insitum', 0),
(111, '111-ABC', '2023-09-12', '21:27:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(115, '4444CAR', '2023-09-12', '21:33:59', '21:35:05', 0, 0.2, 'Insitum', 0),
(116, '4321AFA', '2023-09-12', '21:36:48', '21:36:57', 0, 0, 'reserva planeada', 0),
(117, '3333AAA', '2023-09-12', '21:37:48', '21:37:52', 0, 15, 'reserva rapida', 0),
(118, '111-ABC', '2023-09-12', '21:38:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(120, '111-ABC', '2023-09-12', '21:49:00', '19:09:26', 0, 13, 'reserva rapida', 1),
(121, '111-ABC', '2023-09-17', '09:43:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(122, '111-ABC', '2023-09-17', '10:16:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(124, '111-ABC', '2023-09-17', '10:32:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(125, '111-ABC', '2023-09-17', '16:22:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(126, '111-ABC', '2023-09-17', '16:26:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(127, '111-ABC', '2023-09-17', '16:42:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(128, '111-ABC', '2023-09-17', '16:48:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(129, '111-ABC', '2023-09-17', '16:50:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(130, '111-ABC', '2023-09-17', '16:55:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(131, '111-ABC', '2023-09-17', '16:58:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(132, '111-ABC', '2023-09-17', '17:00:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(133, '111-ABC', '2023-09-17', '17:02:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(134, '111-ABC', '2023-09-17', '17:05:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(135, '111-ABC', '2023-09-17', '18:52:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(136, '1872DDG', '2023-09-19', '18:26:03', '18:40:48', 0, 20, 'reserva rapida', 0),
(140, '111-ABC', '2023-09-19', '18:45:00', '19:09:26', 0, 13, 'reserva rapida', 0),
(141, '1224DDG', '2023-09-19', '18:59:07', '19:15:37', 0, 3.2, 'Insitum', 0),
(144, '4333BAR', '2023-09-20', '07:35:08', '10:34:00', 1, 0, 'reserva rapida', 0),
(145, '3432VAO', '2023-09-20', '07:38:37', '13:38:00', 1, 0, 'Insitum', 0),
(146, '7687REA', '2023-09-20', '07:41:36', '13:00:00', 1, 0, 'Insitum', 0),
(147, '5656GHJ', '2023-09-20', '07:43:29', '18:43:00', 1, 0, 'Insitum', 0);

-- --------------------------------------------------------

--
-- Table structure for table `reserva`
--

CREATE TABLE `reserva` (
  `id_reserva` int(11) NOT NULL,
  `horainiciores` time NOT NULL,
  `fechareserva` date NOT NULL,
  `horafinalres` time NOT NULL,
  `id_cliente` varchar(15) NOT NULL,
  `estado_reserva` int(11) NOT NULL,
  `comentarios` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `reserva`
--

INSERT INTO `reserva` (`id_reserva`, `horainiciores`, `fechareserva`, `horafinalres`, `id_cliente`, `estado_reserva`, `comentarios`) VALUES
(1, '00:00:00', '2023-08-20', '22:53:05', '10101010', 0, 'ninguno'),
(34, '20:41:00', '2023-08-27', '00:00:00', '10101010', 0, ''),
(41, '20:00:00', '2023-09-11', '21:00:00', '10101010', 0, ''),
(42, '20:33:00', '2023-09-21', '22:33:00', '10101010', 1, ''),
(43, '22:34:00', '2023-09-20', '23:34:00', '20202020', 1, ''),
(44, '16:39:00', '2023-09-20', '18:39:00', '50505050', 1, ''),
(45, '13:00:00', '2023-09-20', '15:00:00', '30303030', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `tarifa`
--

CREATE TABLE `tarifa` (
  `idtarifa` int(11) NOT NULL,
  `tipotarifa` varchar(30) NOT NULL,
  `descripciontarifa` varchar(80) NOT NULL,
  `valor` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tarifa`
--

INSERT INTO `tarifa` (`idtarifa`, `tipotarifa`, `descripciontarifa`, `valor`) VALUES
(6, 'reserva planeada', '5% mas de costo normal', 5),
(11, 'Insitum', 'sin previo aviso', 12),
(14, 'reserva rapida', '1 hora antes', 13);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` varchar(15) NOT NULL,
  `contrasena` varchar(11) NOT NULL,
  `nivel` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`idusuario`, `contrasena`, `nivel`) VALUES
('10101010', '111-ABC', 0),
('20202020', '222-ABC', 0),
('30303030', '3333ABC', 0),
('50505050', '1872DDG', 0),
('admin', 'admin', 1),
('enc', 'enc', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`placa`),
  ADD UNIQUE KEY `UK_ci` (`ci`);

--
-- Indexes for table `parqueo`
--
ALTER TABLE `parqueo`
  ADD PRIMARY KEY (`idparqueo`),
  ADD KEY `FK_placa` (`placa`);

--
-- Indexes for table `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`id_reserva`),
  ADD KEY `FK_Cliente_CI` (`id_cliente`);

--
-- Indexes for table `tarifa`
--
ALTER TABLE `tarifa`
  ADD PRIMARY KEY (`idtarifa`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `parqueo`
--
ALTER TABLE `parqueo`
  MODIFY `idparqueo` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT for table `reserva`
--
ALTER TABLE `reserva`
  MODIFY `id_reserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `tarifa`
--
ALTER TABLE `tarifa`
  MODIFY `idtarifa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `parqueo`
--
ALTER TABLE `parqueo`
  ADD CONSTRAINT `FK_placa` FOREIGN KEY (`placa`) REFERENCES `cliente` (`placa`);

--
-- Constraints for table `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `FK_Cliente_CI` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`ci`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
