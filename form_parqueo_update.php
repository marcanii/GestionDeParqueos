<?php
include_once 'conexion.php';
$placa = $_POST['placa'];
$query = "SELECT p.*, c.* FROM parqueo p INNER JOIN cliente c ON p.placa = c.placa WHERE p.placa = '$placa' and p.estado_parqueo = 1";
$row = $con->query($query);

date_default_timezone_set('America/La_Paz');
if ($row->num_rows > 0) {
    //echo $row['ci'];
    $row = $row->fetch_assoc();
    $horaentrada = $row['horaentrada'];
    $observaciones = $row['observaciones'];

    $query2 = "SELECT * FROM tarifa WHERE tipotarifa = '$observaciones'";
    $row2 = $con->query($query2);
    if ($row2->num_rows > 0) {
        $row2 = $row2->fetch_assoc();
        $tipotarifa = $observaciones;
        $valortarifa = $row2['valor'];
    }else{
        $query3 = "SELECT * FROM tarifa WHERE tipotarifa = 'insitum'";
        $row3 = $con->query($query3);
        if ($row3->num_rows > 0) {
            $row3 = $row3->fetch_assoc();
            $tipotarifa = 'insitum';
            $valortarifa = $row3['valor'];
        }
        
    }
    $horaActual = new DateTime();
    $horaSalidaActual = $horaActual->format('H:i:s');
    $horainicioresObj = new DateTime($horaentrada);
    $difHoras = $horainicioresObj->diff($horaActual);

    // Calcula la diferencia total en minutos
    $difTotalMinutos = ($difHoras->h * 60) + $difHoras->i;

    // Convertir esa diferencia en un decimal de horas
    $horasDecimal = $difTotalMinutos / 60;

    // Si estás utilizando la tarifa 'reserva rapida' y has estado menos de una hora, cobra por una hora completa
    if($horasDecimal < 1 && $tipotarifa == 'reserva rapida'){
        $horasDecimal = 1;
    }

    // Multiplica por la tarifa para obtener el costo
    $costo = round($horasDecimal * $valortarifa, 2);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style>
        .bg-custom {
            background-color: rgba(0,0,0,0.7);
        }
        .bg-icon-custom{
            background-color: #5cd2c6;
        }
        .color-text-custom{
            color: #5cd2c6;
        }
        .bg-btn-custom{
            background-color: #5cd2c6;
        }
        .bg-btn-custom:hover{
            background-color: #8ecbcf;
        }
        .text-secondary {
            color: #333;
            text-shadow: 2px 2px 4px #000; /* o cualquier otro color oscuro de tu elección */
}
    </style>
</head>
<body class="bg-dark d-flex justify-content-center align-items-center vh-100">

<div class="row justify-content-center mt-4"> 
    <div class="p-5 rounded-5 text-secondary shadow bg-custom" style="width: 45rem">
        <!-- <div class="d-flex justify-content-center">
            <img src="./assets/add-user.png" width="100" height="100">
        </div> -->
        <div class="text-center fs-1 fw-bold text-white">Liberar vehiculo</div>
        <form action="#" method="post" id="formUpdateParqueo2">
            <div class="row mt-2">

                <div class="col-md-6 form-group">
                    <label for="ci" class= "text-white" >CI:</label>
                    <div class="input-group mt-1">
                        <div class="input-group-text bg-icon-custom">
                            <img src="./assets/ci.png" width="20" height="20">
                        </div>
                        <input class="form-control bg-light" type="number" placeholder="Carnet" name="ci" required value="<?php echo $row['ci']; ?>"/>
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label for="nombres" class= "text-white">Nombres:</label>
                    <div class="input-group mt-1">
                        <div class="input-group-text bg-icon-custom">
                            <img src="./assets/names.png" width="20" height="20">
                        </div>
                        <input class="form-control bg-light" type="text" placeholder="Nombres" name="nombres" required value="<?php echo $row['nombres'] ?>"/>
                    </div>
                </div>
            </div>
    
            <div class="row mt-2">
                <div class="col-md-6 form-group">
                    <label for="apellidos" class= "text-white">Apellidos:</label>
                    <div class="input-group mt-1">
                        <div class="input-group-text bg-icon-custom">
                            <img src="./assets/names.png" width="20" height="20">
                        </div>
                        <input class="form-control bg-light" type="text" placeholder="Apellidos" name="apellidos" required value="<?php echo $row['apellidos'] ?>"/>
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label for="telefono" class= "text-white">Teléfono:</label>
                    <div class="input-group mt-1">
                        <div class="input-group-text bg-icon-custom">
                            <img src="./assets/phone.png" width="20" height="20">
                        </div>
                        <input class="form-control bg-light" type="number" placeholder="Teléfono" name="telefono" required value="<?php echo $row['telefono'] ?>"/>
                    </div>
                </div>
            </div>
    
            <div class="row mt-2">
                <div class="col-md-6 form-group">
                    <label for="placa" class= "text-white">Placa:</label>
                    <div class="input-group mt-1">
                        <div class="input-group-text bg-icon-custom">
                            <img src="./assets/placa.png" width="20" height="20">
                        </div>
                        <input class="form-control bg-light" type="text" placeholder="Placa" name="placa" required value="<?php echo $row['placa'] ?>"/>
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label for="descripcion" class= "text-white">Descripcion Vehículo:</label>
                    <div class="input-group mt-1">
                        <div class="input-group-text bg-icon-custom">
                            <img src="./assets/descrip-vehiculo.png" width="20" height="20">
                        </div>
                        <input class="form-control bg-light" type="text" placeholder="Descripcion Vehículo" name="descripcion" required value="<?php echo $row['descripcionvehiculo'] ?>"/>
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-6 form-group">
                    <label for="fechaentrada" class= "text-white">Hora entrada :</label>
                    <div class="input-group mt-1">
                        <div class="input-group-text bg-icon-custom">
                            <img src="./assets/hora-salida.png" width="20" height="20">
                        </div>
                        <input class="form-control bg-light" type="time" placeholder="Hora de Salida" name="horaentrada" required value="<?php echo $horaentrada ?>"/>
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label for="fechaentrada" class= "text-white">Hora de salida :</label>
                    <div class="input-group mt-1">
                        <div class="input-group-text bg-icon-custom">
                            <img src="./assets/hora-salida.png" width="20" height="20">
                        </div>
                        <input class="form-control bg-light" type="time" placeholder="Hora de Salida" name="horasalida" required value="<?php echo $horaSalidaActual ?>"/>
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label for="fechaentrada" class= "text-white">Tiempo (minutos):</label>
                    <div class="input-group mt-1">
                        <div class="input-group-text bg-icon-custom">
                            <img src="./assets/hora-salida.png" width="20" height="20">
                        </div>
                        <input class="form-control bg-light" type="float" placeholder="Hora de Salida" name="tiempo" required value="<?php echo $difTotalMinutos ?>"/>
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label for="fechasalida" class= "text-white">Estado Parqueo:</label>
                    <div class="input-group mt-1">
                        <div class="input-group-text bg-icon-custom">
                            <img src="./assets/activo.png" width="20" height="20">
                        </div>
                        <select name="estado" id="estado" class="form-control btn-light" required>
                            <option value="" disabled selected>Estado parqueo</option>
                            <option value="1" <?php if ($row['estado_parqueo'] == 1) echo 'selected'; ?>>Activo</option>
                            <option value="0" <?php if ($row['estado_parqueo'] == 0) echo 'selected'; ?>>Inactivo</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-6 form-group">
                    <label for="observaciones" class= "text-white">Tarifa aplicada:</label>
                    <div class="input-group mt-1">
                        <div class="input-group-text bg-icon-custom">
                            <img src="./assets/observaciones.png" width="20" height="20">
                        </div>
                        <input class="form-control bg-light" type="text" placeholder="Observaciones" name="observaciones" required value="<?php echo $row['observaciones'] ?>"/>
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label for="fechaentrada" class= "text-white">Costo Total:</label>
                    <div class="input-group mt-1">
                        <div class="input-group-text bg-icon-custom">
                            <img src="./assets/dinero.png" width="20" height="20">
                        </div>
                        <input class="form-control bg-light" type="number" placeholder="Costo Total" name="costototal" required value="<?php echo $costo; ?>"/>
                    </div>
            </div>
            <input type="button" value="Actualizar" class="btn bg-btn-custom text-white w-25 mt-4 fw-semibold shadow-sm" onclick="updateParqueo()">
        </form>
        <a href="search_parqueo.php" class="btn bg-btn-custom text-white w-25 mt-4" style="margin-left:10px;">Cancelar</a>
        
    </div>
</div>
</body>
</html>
<?php
} else {
    echo "<script>alert('No existe el cliente con la placa $placa'); window.location.href='search_parqueo.php';</script>";
}
?>